////////////////////////////////////////////////////////////////////////////////
// myJs
// Main Controller to add MVC
////////////////////////////////////////////////////////////////////////////////

var MYJS = (typeof MYJS === "undefined") ? {} : MYJS;
MYJS.controller = (typeof MYJS.controller === "undefined") ? {} : MYJS.controller;
MYJS.models = (typeof MYJS.models === "undefined") ? {} : MYJS.models;
MYJS.views = (typeof MYJS.views === "undefined") ? {} : MYJS.views;

/**
  * Represents Main Controller
  * @class
  * @classdesc - This class is for the Main Controller
  * @namespace - MYJS.controller
  * @author - Lawrence Hawker
  * @desc - Main Controller to add MVC
  */
 
// CONTROLLER
MYJS.controller.MainController = function() {
	
	//------------------------------------
	// Variables
	//------------------------------------
	var SETTINGS = null,
		SwipeEventTypes = null,
		ScaleImagesView = null,
		SwipeEventTypesView = null,
		TiledCarouselView = null,
		GATrackingModel = null,
		FormValidation = null;
	
	//------------------------------------
	// Init
	//------------------------------------
	/** 
	  * Represents Initial mass function.
	  * @constructor
	  * @access - Private.
	  */
	function init(){
		createModels();
		createViews();
	}
	
	//------------------------------------
	// Modals
	//------------------------------------
	/** 
	  * Represents Initial creation of models.
	  * @constructor
	  * @access - Private.
	  */
	function createModels(){

		this.SETTINGS = new MYJS.models.WebsiteSettings();
		this.SwipeEventTypes = new MYJS.models.SwipeEventsModel();
		/* Working on developing a tiled carousel model. */
		this.TiledCarousel = new MYJS.models.TiledCarouselModel($("#tiled_carousel_one"), 3, true, "-0px", true);
		
		this.TiledCarousel.setNumberOfSlides(3);
		this.TiledCarousel.resetCarouselOnResize();

		/* Google tracking model */
		this.GATrackingModel = new  MYJS.models.GATrackingModel($(".site"));

		/* A form validation model instance */
		this.FormValidation = new MYJS.models.FormValidationModel(); 
	}

	
	//------------------------------------
	// Views
	//------------------------------------
	/** 
	  * Represents Initial creation of views.
	  * @constructor
	  * @access - Private.
	  */
	function createViews(){

		/* Scale Image View */
		if ($('.site').length > 0) {

			/* Google tracking view instance */
			this.GATrackingView = new  MYJS.views.GATrackingView($(".site"),this.GATrackingModel);

			/* A form validation view instance */
			this.FormValidationView = new  MYJS.views.FormValidationView($(".contact_form"), this.FormValidation);

			this.TiledCarouselView = new MYJS.views.TiledCarouselView($("#tiled_carousel_one"), this.TiledCarousel, this.SwipeEventTypes);
			this.ScaleImagesView = new MYJS.views.ScaleImagesView();

			// loop array and apply image scale to each image.
			if($(".scaleImg").length > 0) {
				var imgArray = $(".scaleImg");

				for (var i=0;i < imgArray.length; i++) { 
					this.ScaleImagesView.scaleThisImage(imgArray[i]);
				}	
			}
		}

		/* TO DO START BUILDING THE SWIPE EVENT TYPE MODEL */
		this.SwipeEventTypesView = new MYJS.views.SwipeEventsView($(".touch_contact"), this.SwipeEventTypes);

	}
	
	init();
	
}

//------------------------------------------------
// Start controller when ready
//------------------------------------------------
if (MYJS.views.FormView) {
	MYJS.application = new MYJS.controller.MainController();
} else {
	$(document).ready(function(e) {
		if (!MYJS.application) MYJS.application = new MYJS.controller.MainController();
	});
}
	