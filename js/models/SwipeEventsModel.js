////////////////////////////////////////////////////////////////////////////////
// A model to return swip events base on the direction of touch events.
// 
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
// Notes, touchEnd can be passed (e, preventX, preventY)
// e: is the current event
// preventX: can be "true / false" this will preventDefault, when on X axis.
// preventY: can be "true / false" this will preventDefault, when on Y axis.
// This class will return the below values to the mouse or tounch event:
// 1) swipeLeft
// 2) swipeRight
// 3) swipeUp
// 4) swipeDown
//------------------------------------------------------------------------------

/**
  * Represents a model for swipe events
  * @class
  * @classdesc - This class is to listen for mouse/touch events and return a swipe status.
  * @namespace - MYJS.model.
  * @author - Lawrence Hawker
  */

MYJS.models.SwipeEventsModel = function() {
	"use strict";

	//--------------------------------------------
	// Public functions
	//--------------------------------------------
	/** 
    * Represents public functions.
    * @constructor
    * @subaccess - Public.
    */
	var _public = {
		/** Represents touch start events
  			* @param {Object} e - Event Object
  			* @desc - public function for touch start events.
  		*/
		touchStart: function(e){
			touchStart(e);
		},
		/** Represents touch end events
  			* @param {Object} e - Event Object
  			* @param {Boolean} preventX - true / false
  			* @param {Boolean} preventY - true / false
  			* @desc - public function for touch end events
  			* @returns {String} returns the direction of the swipe event as a string.
  		*/
		touchEnd: function(e, preventX, preventY) {
			return(touchEnd(e, preventX, preventY));
		}
	};
	
	//--------------------------------------------
	// Private Variables
	//--------------------------------------------
	var startTimeStamp = 0,
		endTimeStamp = 0,
		differenceInTimeStamp = 0,
		touchStartX = null,
		touchStartY = null,
		touchEndX = null,
		touchEndY = null,
		swipeType = null;
	//------------------------------------------------
	// Private Constant variables
	//------------------------------------------------
	var TOUCH_DURATION_CONSTANT = 60,
		DIRECTION_LEFT = "swipeLeft",
		DIRECTION_RIGHT = "swipeRight",
		DIRECTION_UP = "swipeUp",
		DIRECTION_DOWN = "swipeDown",
		IS_MOBILE_SWIPE = (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent));

	//------------------------------------------------
	// Init
	//------------------------------------------------
	/** 
    * Represents Initial mass function.
    * @constructor
    * @access - Private.
    */
	function init() {
		if (typeof console == "object") {
      		console.log("SwipeEventsModel loaded.");
    	}
	}
	
	//--------------------------------------------
	// Touch started
	//--------------------------------------------
	/** 
    * Represents touchStart function.
    * @constructor
    * @access - Private.
    * @param {Object} e - Event Object
    */
	function touchStart(e) {
		if(e.type === "mousedown" || e.type === "touchstart") /*This will become touch */ {
			startTimeStamp = e.timeStamp;
			if(IS_MOBILE_SWIPE){
				touchStartX = e.originalEvent.touches[0].clientX;
				touchStartY = e.originalEvent.touches[0].clientY;
			} else {
				touchStartX = e.originalEvent.clientX;
				touchStartY = e.originalEvent.clientY;
			}
		}
	}

	//--------------------------------------------
	// Touch ended
	//--------------------------------------------
	/** 
    * Represents touchEnd function.
    * @constructor
    * @access - Private.
    * @param {Object} e - Event Object
    * @param {Boolean} preventX - true / false
    * @param {Boolean} preventY - true / false
    */
	function touchEnd(e, preventX, preventY) {

		if(e.type === "mouseup" || e.type === "touchmove") {
			
			endTimeStamp = e.timeStamp;
			/* Prevent defaults when swiping left or right */
			if(preventX){
				if(swipeType === "swipeLeft" || swipeType === "swipeRight") {
					e.preventDefault();
					e.stopPropagation();
				}
			}
			/* Prevent defaults when swiping up or down */
			if(preventY) {
				if(swipeType === "swipeUp" || swipeType === "swipeDown") {
					e.preventDefault();
					e.stopPropagation();
				}
			}
			
			if(IS_MOBILE_SWIPE){ 
				touchEndX = e.originalEvent.touches[0].clientX;
				touchEndY = e.originalEvent.touches[0].clientY;

			} else {
				touchEndX = e.originalEvent.clientX;
				touchEndY = e.originalEvent.clientY;
			}

			differenceInTimeStamp = (endTimeStamp - startTimeStamp);
			/* Check to see if we have a swipe happening */
			if(differenceInTimeStamp >= TOUCH_DURATION_CONSTANT) {
			/* left do a function call to check direction of swipe :) */
				swipeType = swipeDirecton(touchStartX, touchStartY, touchEndX, touchEndY);
			}
		}

		if(e.type === "touchend" || e.type === "mouseup") {
			return swipeType;
			$(e).off("mouseup touchend touchmove");
		}
	}

	//------------------------------------------------
	// Workout swipe direction and return data.
	//------------------------------------------------
	/** 
    * Represents swipeDirecton function.
    * @constructor
    * @access - Private.
    * @param {Number} touchSX - Numeric start event pageX
    * @param {Number} touchSY - Numeric start event pageY
    * @param {Number} touchEX - Numeric end event pageX
    * @param {Number} touchEY - Numeric end event pageY
    * @returns {String} Returns the last swipe event direction
    */
	function swipeDirecton(touchSX, touchSY, touchEX, touchEY){
		var x = Math.abs(touchSX - touchEX);
		var y = Math.abs(touchSY - touchEY);
		if(x >= y) {
			return touchSX - touchEX > 0 ? DIRECTION_LEFT : DIRECTION_RIGHT;
		}
			return touchSY - touchEY > 0 ? DIRECTION_UP : DIRECTION_DOWN;
	}

	init();
	return _public;
	
}