////////////////////////////////////////////////////////////////////////////////
// myJs
// Setting used throughout the site
////////////////////////////////////////////////////////////////////////////////

/**
  * Represents Website Settings
  * @class
  * @classdesc - This class is a place to define constant variables.
  * @namespace - MYJS.models
  * @author - Lawrence Hawker
  */
 
MYJS.models.WebsiteSettings = function() {
	"use strict";

	var _public = {
		
		//------------------------------------------------
		// Constant variables
		//------------------------------------------------
		IMG_SCALE_SIZE_SMALL: 200,
	};

	return _public;
	
}