/**
  * Represents a model to add a tiled carousel
  * @class
  * @classdesc - This class is for a tiled carousel
  * @namespace - MYJS.model.
  * @param {Object} selector - Jquery object.
  * @param {Number} numberOfSlides - Number of slides
  * @param {Boolean} navigation - true / false
  * @param {String} offset - "-0px" to offset the slides to the left.
  * @param {Boolean} pagignation - true / false to show the pagignation dots
  * @author - Lawrence Hawker
  * @desc - The tiled carousel will scroll over all images
  * it will also make use of the SwipEventsModel to detect
  * touch events.  
  */
MYJS.models.TiledCarouselModel = function(selector, numberOfSlides, navigation, offset, pagignation) {
  "use strict";

  //--------------------------------------------
  // Public functions
  //--------------------------------------------
  /** 
    * Represents public functions.
    * @constructor
    * @subaccess - Public.
    */
  var _public = { 
    /** 
    * Represents public function (setNumberOfSlides).
    * @constructor
    * @subaccess - Public.
    * @param {Number} - Numeric value.
    * @desc Used to set how many slide should be display 
    * on screen at anyone time.
    */
    setNumberOfSlides: function(num){
      if(num > 0) {
        numberOfSlidesToShow = num;
      }
    },
    /** 
    * Represents public function (resetCarouselOnResize).
    * @constructor
    * @subaccess - Public.
    * @desc Reset the size of the carousel on when called.
    */
    resetCarouselOnResize: function(){
      resetCarousel();
    },
    /** 
    * Represents public function (animationCarousel).
    * @constructor
    * @subaccess - Public.
    * @param {Object} e - Event Object.
    * @param {String} direction - String value.
    * @desc Animate the carousel.
    */
    animationCarousel: function(e, direction) {
        carouselAnimationDirection(e, direction); 
    }

  };

  //--------------------------------------------
  // Private Variables
  //--------------------------------------------
  /** 
    * @subaccess - Private.
    * @desc Private variable declaration
    */
  var SELECTOR = null,
      SLIDES_SELECTOR = null,
      smallCarousel = null,
      getCarouselContainerWidth = null,
      CarouselContentWidth = null,
      reorderCarouselItems = null,
      numberOfSlidesToShow = null,
      carouselChildren = null,
      showNavigation = null,
      marginOffset = null,
      showPagignation = null,
      selectedPagignationItem = null,
      left_indent = 0;

  //------------------------------------------------
  // Init
  //------------------------------------------------
  /** 
    * Represents Initial mass function.
    * @constructor
    * @access - Private.
    */
  function init() {

    if (typeof console == "object") {
      console.log("TiledCarouselModel loaded.");
    }

    SELECTOR = $(selector);
    numberOfSlidesToShow = numberOfSlides;
    showNavigation = navigation;
    showPagignation = pagignation;
    marginOffset = offset;
    left_indent = 0;
    selectedPagignationItem = 0;
    carouselChildren = SELECTOR.find(".tiled_carousel_ul li")
    /* call on them functions */
    createListeners();
    setSliderBackground();
    carouselNavigationOptions();
    carouselVisualLayout();
    carouselPagignation();
    currentSlideNumericIndicator();
    resetCarousel();
  }

  //-------------------------------------------
  // Create Event Listeners
  //-------------------------------------------
  /** 
    * Represents function to listen for events.
    * @constructor
    * @access - Private.
    */
   function createListeners() {
      
      SELECTOR.find(".right_scroll").on("click", function(e) { 
        carouselAnimationDirection(e, "swipeRight"); 
      });

      SELECTOR.find(".left_scroll").on("click", function(e) { 
        carouselAnimationDirection(e, "swipeLeft"); 
      });
   }

  //-------------------------------------------
  // Reset the carousel
  //-------------------------------------------
  /** 
    * Represents function to reset the carousel after window resize..
    * @constructor
    * @access - Private.
    */
  function resetCarousel(){
    carouselVisualLayout();
  }

  //-------------------------------------------
  // Carousel navigation show / hide
  //-------------------------------------------
  /** 
    * Represents Show next & prev navigation function.
    * @constructor
    * @access - Private.
    */
  function carouselNavigationOptions() {
    if(showNavigation) {
      SELECTOR.find(".tiled_carousel_navigation").show();
    } else {
      SELECTOR.find(".tiled_carousel_navigation").hide();
    }
  }
  
  //-------------------------------------------
  // Add pagination
  // ------------------------------------------
  /** 
    * Represents set carousel visual style function.
    * @constructor
    * @access - Private.
    * @desc Shows a current item pagination dot.
    */
  function carouselPagignation(){

      //var total = carouselChildren.length / numberOfSlidesToShow;
      var total = carouselChildren.length;
      
      if(showPagignation) {
        SELECTOR.find(".pagination").show();
        
      } else {
        SELECTOR.find(".pagination").hide();
      }

      for (var i=0;i < total; i++) { 
        SELECTOR.find(".pagination ul").append("<li>&nbsp;</li>");
      }

      var pagignationChildren = SELECTOR.find(".pagination li");
      $(pagignationChildren[selectedPagignationItem]).addClass('active');

  }

  //-------------------------------------------
  // Current pagignation selected
  //-------------------------------------------
  /** 
    * Represents set pagignation to match current slide.
    * @constructor
    * @access - Private.
    * @desc Changes the pagination marker to match current slide.
    */
  function changeCarouselPagignation(){

    var pagignationChildren = SELECTOR.find(".pagination li");
    
    //if(Math.abs(selectedPagignationItem) === Math.round(carouselChildren.length / numberOfSlidesToShow)) {
    if(Math.abs(selectedPagignationItem) === Math.round(carouselChildren.length)) {
        selectedPagignationItem = 0;
    }

    $(pagignationChildren).removeClass('active');
    $(pagignationChildren[Math.abs(selectedPagignationItem)]).addClass('active');
  }

  //-------------------------------------------
  // Current slide numeric indicator
  //-------------------------------------------
  /** 
    * Represents function to Numerically indicat current selection.
    * @constructor
    * @access - Private.
    */
  function currentSlideNumericIndicator(){

      if(Math.abs(selectedPagignationItem) === 0) {
        var currentCounter = 1;
      } else {
        var currentCounter = Math.abs(selectedPagignationItem) + 1;
      }

      SELECTOR.find(".tiled_carousel_counter .current_count").html(currentCounter);
      SELECTOR.find(".tiled_carousel_counter .total_count").html(Math.round(carouselChildren.length));
  }
    
  //-------------------------------------------
  // Carousel visual layout
  //-------------------------------------------
  /** 
    * Represents set carousel visual style function.
    * @constructor
    * @access - Private.
    */
  function carouselVisualLayout(){
    setCarouselValues();
    SELECTOR.find(".tiled_carousel_inner").width($(window).outerWidth());
    SELECTOR.find(".tiled_carousel_ul").css({"margin-left" : marginOffset });
    SELECTOR.find(".tiled_carousel_ul li").width($(window).outerWidth() / numberOfSlidesToShow);
      
    // uncomment the below if we want to reoder the slides
    SELECTOR.find(".tiled_carousel_ul li:first").before(SELECTOR.find('.tiled_carousel_ul li:last'));
  }

  //-------------------------------------------
  // Carousel set visual values
  //-------------------------------------------
  function setCarouselValues(){
    var setDefaultLeftOffset = "-" + $(window).width() / numberOfSlidesToShow + "px";
    SELECTOR.find(".tiled_carousel_ul").css({"left" : setDefaultLeftOffset});  
    /* The below does not look to be in uses by anything */
    /* getCarouselContainerWidth = SELECTOR.find(".slide-container").width(); 
    SLIDES_SELECTOR = SELECTOR.find(".slide-container .slides").children();
    CarouselContentWidth = $(window).width() * SLIDES_SELECTOR.length; */
  }

  //-------------------------------------------
  // Set the background img for each silder
  //-------------------------------------------
  /** 
    * Represents set background image function.
    * @constructor
    * @access - Private.
    */
   function setSliderBackground() { 
      var imgURL;
      if(carouselChildren.length > 0) {
        /* loop over carousel children */
        for (var i=0;i < carouselChildren.length; i++) { 
          imgURL = $(carouselChildren[i]).data("image");
          if(imgURL) {
            $(carouselChildren[i]).css({'background-image' : 'url("' + imgURL + '")'});
          }
        }
      }
   }

  //-------------------------------------------
  // Carousel direction to animation
  //-------------------------------------------
  /** 
    * Represents function to animate the carousel.
    * @constructor
    * @subaccess - Private.
    * @param {Object} e - Passed event object.
    * @param {String} direction - swipeLeft / swipeRight.
    * @desc As the SwipeEventModel is used we must use swipeLeft / swipeRight
    */
  function carouselAnimationDirection(e, direction) {
    var item_width = SELECTOR.find(".tiled_carousel_ul li").width();

    //calculae the new left indent of the un-ordered list
    if(direction === "swipeLeft") {
      left_indent = parseInt(SELECTOR.find(".tiled_carousel_ul").css("left")) - item_width;
      reorderCarouselItems = "after";
      selectedPagignationItem = selectedPagignationItem - 1;
    } else if (direction === "swipeRight") {
      left_indent = parseInt(SELECTOR.find(".tiled_carousel_ul").css("left")) + item_width;
      reorderCarouselItems = "before";
      selectedPagignationItem = selectedPagignationItem + 1;
    }

    // now animate
    SELECTOR.find(".tiled_carousel_ul:not(:animated)").stop().animate({"left" : left_indent},500,function(){ 
       carouselInfiniteAnimation(reorderCarouselItems); 
    });

    changeCarouselPagignation();
    currentSlideNumericIndicator();

  }

  //-------------------------------------------
  // Carousel infinite animation and reorder
  //-------------------------------------------
  /** 
    * Represents function to animate an infinite number of times.
    * @constructor
    * @subaccess - Private.
    * @param {String} e - The order to show carousel slides.
    * @desc Infinite animation and reordering on carousel slides.
    */
  function carouselInfiniteAnimation(order){ 
    if(order === "after") {
         SELECTOR.find(".tiled_carousel_ul li:last").after(SELECTOR.find(".tiled_carousel_ul li:first")); 
      } else if(order === "before") {
        SELECTOR.find(".tiled_carousel_ul li:first").before(SELECTOR.find(".tiled_carousel_ul li:last")); 
      }
      
      //Set the left indent to the default pixel value
      $(".tiled_carousel_ul").css({"left" : "-" + $(window).outerWidth() / numberOfSlidesToShow + "px"});
  }

  init();
  return _public;
}