////////////////////////////////////////////////////////////////////////////////
// myJs
// Google tracking events model
////////////////////////////////////////////////////////////////////////////////
/**
  * Represents a model to handel Google analytics tracking.
  * @class
  * @classdesc - A class to use for the tracking of links with Google analytics.
  * @namespace - MYJS.models
  * @author - Lawrence Hawker
  */
 
 MYJS.models.GATrackingModel = function(selector) {
	"use strict";

	//--------------------------------------------
	// Public functions
	//--------------------------------------------
	/**
		* Represents public functions.
		* @constructor
		* @subaccess - Public.
		* @namespace - MYJS.model.
		* @param {Object} event - Event object.
		* @param {String} category - passed category string.
		* @param {String} label - passed label string.
	*/
	var _public = {
		sendTrackingEvent : function(event, category, label) {
			ga_eventType = event.type;
			ga_category = category;
			ga_label = label;

			if(!ga_label) {
				/*  get the last string from URL and pass it as lable */
				ga_label = event.target.href.split(/[/ ]+/).pop();
			}

			gaTrackingEvents();
		}
	};
	
	//--------------------------------------------
	// Private Variables
	//--------------------------------------------
	var SELECTOR = null,
		pathArray = null,
		lastItem = null,
		ga_category = null,
		ga_label = null,
		ga_eventType = null;

	//------------------------------------------------
	// Init
	//------------------------------------------------
	/** 
		* Represents Initial mass function.
		* @constructor
		* @access - Private.
	*/
	function init() {
		if (typeof console == "object") {
      		console.log("Google analytics tracking model loaded.");
    	}
    	SELECTOR = $(selector);
	}

	//------------------------------------------------
	// Watch for GA tracking events
	//------------------------------------------------
	/** 
		* Represents public function to send Google analytics tracking events.
		* @constructor - gaTrackingEvents
		* @access - Private.
	*/
	function gaTrackingEvents() {
		if (typeof ga !== 'undefined') {
			//alert("ga_category " + ga_category + " ga_eventType " + ga_eventType + " ga_label " + ga_label);
			ga('send', 'event', ga_category, ga_eventType, ga_label);
		}
	}

	init();
	return _public;
}