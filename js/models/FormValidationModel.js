////////////////////////////////////////////////////////////////////////////////
// myJs
// Form Validation Model
////////////////////////////////////////////////////////////////////////////////
/**
  * Represents a model for form validation.
  * @class
  * @classdesc - A class used to validate form fields.
  * @namespace - MYJS.models
  * @author - Lawrence Hawker
  */
 
 MYJS.models.FormValidationModel = function(selector) {
	"use strict";

	//--------------------------------------------
	// Public functions
	//--------------------------------------------
	/**
		* Represents public functions.
		* @constructor - validateData.
		* @subaccess - Public.
		* @namespace - MYJS.model.
		* @param {Object} e - Event object.
		* @param {String} data - passed form data.
	*/
	var _public = {
		validateData : function(selector, e, data) {
			if(!cookieSet) {
				e.stopPropagation();
				e.preventDefault();
			}
			
			validateForm(selector, e, data);
		},

		focusInClearField: function(selector) {
			clearFieldData(selector);
		},

		focusOutClearField: function(selector) {
			resetFieldData(selector);
		}
	};
	
	//--------------------------------------------
	// Private Variables
	//--------------------------------------------
	var dataArray = null,
		errorString = null,
		validationArray = null,
		cookieSet = false,
		validForm = false;


	//------------------------------------------------
	// Init
	//------------------------------------------------
	/** 
		* Represents Initial mass function.
		* @constructor
		* @access - Private.
	*/
	function init() {
		if (typeof console == "object") { 
			console.log("Form validation model loaded"); 
		}
		errorString = "This field has to be filled";
	}
	//------------------------------------------------
	// Clear form field data
	//------------------------------------------------
	/**
		* Represents clearFieldData functions.
		* @constructor - clearFieldData.
		* @subaccess - Private.
		* @param {Object} selector - jQuery object
		* @desc - clear the selected field of it's value.
	*/
	function clearFieldData(selector) {
		var data = $(selector).val()
		if(data === "" || data ==+ " " || data === $(selector).data("default") || data === errorString) {
			$(selector).val("");
		}
	}

	//------------------------------------------------
	// Reset the input text
	//------------------------------------------------
	/**
		* Represents resetFieldData functions.
		* @constructor - resetFieldData.
		* @subaccess - Private.
		* @param {Object} selector - jQuery object
		* @desc - Reset the field to the default value.
	*/
	function resetFieldData(selector) {
		var data = $(selector).val()
		if(data === "" || data ==+ " " || data === $(selector).data("default") || data === errorString) {
			$(selector).val($(selector).data("default"));
		} else {
			$(selector).val($(selector).val());
		}
	}

	//------------------------------------------------
	// Vaildate the current postted form data
	//------------------------------------------------
	/**
		* Represents validateForm functions.
		* @constructor - validateForm.
		* @subaccess - Private.
		* @param {Object} selector - jQuery object
		* @param {Object} event - Event object
		* @param {Array} data - Array of data
		* @desc - Validation of all mandatory fields 
	*/
	function validateForm(selector, event, data) {
		validationArray = new Array();
		if(data.length > 0 ) {
			jQuery.each( data, function( i, data ) {

				/* check only required fields */
				if($(selector).find("[name= "+data.name+"]").data("required") || $(selector).find("[name= "+data.name+"]").data("type")) {
					//console.log("REQUIRED");
					if($(selector).find("[name= "+data.name+"]").data("default")) {
						//console.log($(selector).find("[name= "+data.name+"]").data("default"));
						
						if(validationError($(selector).find("[name= "+data.name+"]"), $(selector).find("[name= "+data.name+"]").data("default"), $(selector).find("[name= "+data.name+"]").val())) {
							//console.log("passed the test move on");
							/* need to do the pased test stuff now.*/
							$(selector).find("[name= "+data.name+"]").removeClass("error");
							validationArray.push("passed");
						} else {
							$(selector).find("[name= "+data.name+"]").val(errorString);
							$(selector).find("[name= "+data.name+"]").addClass("error");
							validationArray.push("failed");
						}
					}
					/* check data type matchies input value */
					acceptedDataTypeText($(selector).find("[name= "+data.name+"]"), $(selector).find("[name= "+data.name+"]").data("type"), $(selector).find("[name= "+data.name+"]").val());
				}
		    });
		} else {
			if (typeof console == "object") {
				console.log("ERROR no data");
			}
		}

		var i = validationArray.indexOf("failed");

		if(i <  0) {
			validForm = true;
		} else {
			validForm = false;
		}

		if(validForm) {
			
			if(readCookie("dataSent")) {
				postData(selector);
				cookieSet = true;
			} else {
				console.log("dont post");
				cookieSet = false;
			}
			
		}
	}

	//------------------------------------------------
	// Field data types to validate
	//------------------------------------------------
	/**
		* Represents acceptedDataTypeText functions.
		* @constructor - acceptedDataTypeText.
		* @subaccess - Private.
		* @param {Object} selector - jQuery object
		* @param {String} type - Fields type text/email so on
		* @param {String} value - Fields data value
		* @desc - Validation mandatory fields based on their type. 
	*/
	function acceptedDataTypeText(selector, type, value){
		/* check for text string */
		if(type === "text" && !isNaN(value)) {
			$(selector).addClass("invalid_type");
			validationArray.push("failed");
		} else {
			//$(selector).removeClass("invalid_type");
		}

		/* check for valid email address */
		if(type === "email") {
			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm;
			if ($(selector).val() == '' || !re.test($(selector).val())) {
				$(selector).addClass("invalid_type");
				validationArray.push("failed");
			}
		} else {
			//$(selector).removeClass("invalid_type");
		}
	}

	//------------------------------------------------
	// Data is valid now post data
	//------------------------------------------------
	/**
		* Represents postData functions.
		* @constructor - postData.
		* @subaccess - Private.
		* @param {Object} selector - jQuery object
		* @desc - Function to post the form after a short timeout. 
	*/
	function postData(selector) {
		setTimeout( function () { $(selector).submit(); }, 400);
	}

	//------------------------------------------------
	// Set a cookie & prevent multiple form submissions
	//------------------------------------------------
	/**
		* Represents setCookie functions.
		* @constructor - setCookie.
		* @subaccess - Private.
		* @param {String} key - Name of the cookie
		* @desc - Function to set a cookie
	*/
	function setCookie(key) {
		var expireDate = new Date();
		/* (70*60*1000); */
		expireDate.setTime(expireDate.getTime()+(1*60*1000)); 	
		var date = Date.UTC(expireDate.getFullYear(), expireDate.getMonth(), expireDate.getDate(), expireDate.getHours(), expireDate.getMinutes(), expireDate.getSeconds(), expireDate.getMilliseconds());
		document.cookie = escape(key) + "=" + expireDate.toGMTString() + "; expires="+ expireDate.toGMTString() + "; path=/";
	}

	//-------------------------------------------------
	// Check if cookie has been dropped
	//------------------------------------------------
	/**
		* Represents readCookie functions.
		* @constructor - readCookie.
		* @subaccess - Private.
		* @param {String} key - Name of the cookie
		* @returns {boolean} true/false
		* @desc - Function to read the cookie if set
	*/
	function readCookie(key) {
		/* var keyValue = document.cookie.match("(^|;) ?" + key + "=([^;]*)(;|$)"); */
		var keyValue = document.cookie.match(new RegExp(key + '=([^;]+)'));

		if(!keyValue) {
			setCookie("dataSent");
			return true;
		} else {
			return false;
		}
	}

	//------------------------------------------------
	// Watch for validation errors
	//------------------------------------------------
	/**
		* Represents validationError functions.
		* @constructor - validationError.
		* @subaccess - Private.
		* @param {Object} selector - jQuery object
		* @param {String} defaultValue - data-default value
		* @param {String} Value - objects value
		* @returns {boolean} true/false
		* @desc - Returns a validation error
	*/
	function validationError(selector, defaultValue, Value){
		if(defaultValue && Value) {
			if(Value === defaultValue || Value.replace(/ /g, "") === "" || Value === errorString){
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	init();
	return _public;
}