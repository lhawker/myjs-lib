/**
  * Represents a view of a tiled carousel
  * @class
  * @classdesc - This class is for a tiled carousel view.
  * @namespace - MYJS.views.
  * @param {Object} selector - Jquery object.
  * @param {Model Object} carouselModel - Class model object.
  * @param {Model Object} swipeEventModel - Class model object.
  * @author - Lawrence Hawker
  * @desc - Create a tiled carousel and listen for swipe events.
  */

MYJS.views.TiledCarouselView = function(selector, carouselModel, swipeEventModel) {

  "use strict";

  //--------------------------------------------
  // Public functions
  //--------------------------------------------
  /** 
    * Represents public functions.
    * @constructor
    * @subaccess - Public.
    */
  var _public = { };

  //------------------------------------------------
  // Private Variables
  //------------------------------------------------
  var SELECTOR = null;

  //------------------------------------------------
  // Init
  //------------------------------------------------
  /** 
    * Represents Initial mass function.
    * @constructor
    * @access - Private.
    */
  function init() {
    if (typeof console == "object") {
          console.log("TiledCarouselView loaded.");
    }
    SELECTOR = $(selector);
    createListeners();
  }

  //-------------------------------------------
  // Create Event Listeners
  //-------------------------------------------
  /** 
    * Represents Listen for events function.
    * @constructor
    * @access - Private.
    */
  function createListeners(){
    /* SWIPE */
    SELECTOR.on("mousedown touchstart", function(e) {swipeEventModel.touchStart(e);});

    SELECTOR.on("mouseup touchmove touchend", function(e) { 
      // values to pass (event, preventX, preventY)
      carouselModel.animationCarousel(e, swipeEventModel.touchEnd(e, true, false)); 
    });

    $(window).on("resize", function(e){onWindowResized(e);});
  }

  //-------------------------------------------
  // Change the number of visable carousel slides
  //-------------------------------------------
  /** 
    * Represents function to change the number of visable slides.
    * @constructor
    * @access - Private.
    */
  function changeMobileCarouselSlideValue(){
    if($(window).width() <= 800) {
      carouselModel.setNumberOfSlides(1);
    } else {
      carouselModel.setNumberOfSlides(3);
    }
    carouselModel.resetCarouselOnResize();
  }

  // On window resize
  //-------------------------------------------
  /** 
    * Represents Window resize event function.
    * @constructor
    * @access - Private.
    */
  function onWindowResized(e) {
    changeMobileCarouselSlideValue();
  }

  init();
  return _public;
}