////////////////////////////////////////////////////////////////////////////////
// myJs
// Google tracking events view.
////////////////////////////////////////////////////////////////////////////////
/**
  * Represents a view to handel Google analytics tracking.
  * @class
  * @classdesc - A class to listen fo tracking events.
  * @namespace - MYJS.views
  * @param {Object} selector - Jquery object.
  * @param {Model Object} carouselModel - Class model object.
  * @param {Model Object} swipeEventModel - Class model object.
  * @author - Lawrence Hawker
  */
 
 MYJS.views.GATrackingView = function(selector, GATrackingModel) {

  "use strict";

  //--------------------------------------------
  // Public functions
  //--------------------------------------------
  /** 
    * Represents public functions.
    * @constructor
    * @subaccess - Public.
    */
  var _public = { };

  //------------------------------------------------
  // Private Variables
  //------------------------------------------------
  var SELECTOR = null;

  //------------------------------------------------
  // Init
  //------------------------------------------------
  /** 
    * Represents Initial mass function.
    * @constructor
    * @access - Private.
    */
  function init() {
    if (typeof console == "object") {
          console.log("Google analytics tracking view loaded.");
    }
    SELECTOR = $(selector);
    createListeners();
  }

  //-------------------------------------------
  // Create Event Listeners
  //-------------------------------------------
  /** 
    * Represents Listen for events function.
    * @constructor
    * @access - Private.
    */
  function createListeners(){
    /* track click events */
    SELECTOR.find("a").on("click", function(e) {GATrackingModel.sendTrackingEvent(e, "Main-Navigation", "label text string");});
				
  }

  init();
  return _public;
}