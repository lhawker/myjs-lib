/**
  * Represents a view to listen for swipe events
  * @class
  * @classdesc - This class is for the swipe evnts view.
  * @namespace - MYJS.views.
  * @param {Object} selector - Jquery object.
  * @param {Object} selector - Class model object.
  * @author - Lawrence Hawker
  * @desc - This view will call the Swipe events model 
  *	and have returned the swip event type.
  */

MYJS.views.SwipeEventsView = function(selector, model) {
	
	"use strict";

	//--------------------------------------------
	// Public functions
	//--------------------------------------------
	var _public = { };

	//--------------------------------------------
	// Private Variables
	//--------------------------------------------
	var SELECTOR = null,
		scope = this;
	//--------------------------------------------
	// Init function
	//--------------------------------------------
	/** 
	  * Represents Initial mass function.
	  * @constructor
	  * @access - Private.
	  */
	function init() {
		if (typeof console == "object") {
      		console.log("SwipeEventsView loaded.");
    	}

		scope.SELECTOR = $(selector);
		createListeners();
	}

	//-------------------------------------------
	// Create Event Listeners
	//-------------------------------------------
	/** 
	  * Represents Listen for events function.
	  * @constructor
	  * @access - Private.
	  */
	function createListeners(){
		/* SWIPE */
		scope.SELECTOR.on("mousedown touchstart", function(e) { model.touchStart(e); });

		scope.SELECTOR.on("mouseup touchmove touchend", function(e) { 
			// values to pass (event, preventX, preventY)
			$(".show_swipe_directions").html(model.touchEnd(e, true, false));
		});
	}
	
	init();
	return _public;
}