/**
  * Represents a view for the Scaling of images
  * @class
  * @classdesc - This class is for the image scale view.
  * @namespace - MYJS.views.
  * @author - Lawrence Hawker
  * @desc - this will become a class which when passed
  	an image and width dimention will scale an image
	and keeps its aspact and the return the new image
	sizes. JSDoc
  */
MYJS.views.ScaleImagesView = function() {
	
	"use strict";

	var _public = {
	/** 
	  * Represents public function to scale the passed image object.
	  * @constructor
	  * @subaccess - Public.
	  * @param {Object} selector - Jquery object.
	  */
		scaleThisImage: function(selector) {

			this.SELECTOR = $(selector);
			this.IMAGE = this.SELECTOR.find("img");
			/* Set private variables */
			IMAGE_WIDTH = this.IMAGE.width();
			IMAGE_HEIGHT = this.IMAGE.height();
			TARGET_WIDTH = this.SELECTOR.innerWidth();
			TARGET_HEIGHT = this.SELECTOR.innerHeight();
			var result = ScaleImage(IMAGE_WIDTH, IMAGE_HEIGHT, TARGET_WIDTH, TARGET_HEIGHT, true);
			/* Adjust the image coordinates and size */
    		this.SELECTOR.width(result.width);
    		this.IMAGE.height(result.height);
    		this.IMAGE.css("left", result.targetleft);
    		this.IMAGE.css("top", result.targettop);
		}
	};

	
	//--------------------------------------------
	// Private Variables
	//--------------------------------------------
	var SELECTOR = null,
		scope = this,
		IMAGE_WIDTH = null,
		IMAGE_HEIGHT = null,
		TARGET_WIDTH = null,
		TARGET_HEIGHT = null;
	
	//--------------------------------------------
	// Init function
	//--------------------------------------------
	/** 
	  * Represents Initial mass function.
	  * @constructor
	  * @access - Private.
	  */
	function init() {
		if (typeof console == "object") {
      		console.log("ScaleImagesView loaded.");
    	}
	}

	//--------------------------------------------
	// ScaleImage function takes 5 parameters:
	//--------------------------------------------
	/** 
	  * Represents scale function.
	  * @constructor
	  * @access - Private.
	  * @param {Number} srcwidth - Integer width of the source image to scale.
	  * @param {Number} srcheight - Integer height of the source image to scale.
	  * @param {Number} targetwidth - Integer width of the parent element that we want to fit the image into.
	  * @param {Number} targetheight - Integer height of the parent element that we want to fit the image into.
	  * @param {Boolean} fLetterBox - If we want the “fitted” mode. Otherwise, the function uses the “zoom” mode.
	  * @returns {Number} result - Sum of targetwidth - result.width and targetheight - result.height.
	  */

	function ScaleImage(srcwidth, srcheight, targetwidth, targetheight, fLetterBox) {

		var result = { width: 0, height: 0, fScaleToTargetWidth: true };

		if ((srcwidth <= 0) || (srcheight <= 0) || (targetwidth <= 0) || (targetheight <= 0)) {
        	return result;
    	}

	    /* Scale to the target width */
	    var scaleX1 = targetwidth;
	    var scaleY1 = (srcheight * targetwidth) / srcwidth;

	    /* Scale to the target height */
	    var scaleX2 = (srcwidth * targetheight) / srcheight;
	    var scaleY2 = targetheight;

	    /* Now figure out which one we should use */
	    var fScaleOnWidth = (scaleX2 > targetwidth);
	    if (fScaleOnWidth) {
	        fScaleOnWidth = fLetterBox;
	    }
	    else {
	       fScaleOnWidth = !fLetterBox;
	    }

	    if (fScaleOnWidth) {
	        result.width = Math.floor(scaleX1);
	        result.height = Math.floor(scaleY1);
	        result.fScaleToTargetWidth = true;
	    }
	    else {
	        result.width = Math.floor(scaleX2);
	        result.height = Math.floor(scaleY2);
	        result.fScaleToTargetWidth = false;
	    }

	    result.targetleft = Math.floor((targetwidth - result.width) / 2);
	    result.targettop = Math.floor((targetheight - result.height) / 2);

	    return result;
	}

	init();
	return _public;
}