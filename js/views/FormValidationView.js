////////////////////////////////////////////////////////////////////////////////
// myJs
// Form Validation view
////////////////////////////////////////////////////////////////////////////////

MYJS.views.FormValidationView = function(selector, FormValidationModel) {

  "use strict";

  //--------------------------------------------
  // Public functions
  //--------------------------------------------
  /** 
    * Represents public functions.
    * @constructor
    * @subaccess - Public.
    */
  var _public = { };

  //------------------------------------------------
  // Private Variables
  //------------------------------------------------
  var SELECTOR = null,
      scope = this;

  //------------------------------------------------
  // Init
  //------------------------------------------------
  /** 
    * Represents Initial mass function.
    * @constructor
    * @access - Private.
    */
  function init() {
    if (typeof console == "object") {
          console.log("Form validation view loaded.");
    }
    SELECTOR = $(selector);
    createListeners();
  }

  //-------------------------------------------
  // Create Event Listeners
  //-------------------------------------------
  /** 
    * Represents Listen for events function.
    * @constructor
    * @access - Private.
    */
  function createListeners(){
    SELECTOR.find("form").on("submit", function(e) { 
      var dataArray = $(this).serializeArray();
      FormValidationModel.validateData($(this), e, dataArray);
    });

    SELECTOR.find("input").on({
      focusin:function(e) { 
        FormValidationModel.focusInClearField($(this)); 
      }, 
      focusout:function(e){ 
        FormValidationModel.focusOutClearField($(this));
      }
    });
  }

  init();
  return _public;
}